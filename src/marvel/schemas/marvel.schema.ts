import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as moment from 'moment';
import { SearchType } from '../enums/search-type.enum';

export type MarvelDocument = Marvel & Document;

@Schema()
export class Marvel {

  @Prop({
    required:true,
    default: moment().format()
  })
  lastSync: string;

  @Prop({
      required:true,
      enum: SearchType
  })
  searcType: string;

  @Prop({
      required:true,
      maxlength:450
  })
  character: string;

}

export const MarvelSchema = SchemaFactory.createForClass(Marvel);