import { MongooseModule } from '@nestjs/mongoose';
import { HttpModule, Module } from '@nestjs/common';
import { MarvelService } from './marvel.service';
import { MarvelController } from './marvel.controller';
import { Marvel, MarvelSchema } from './schemas/marvel.schema';

@Module({
  imports:[
    HttpModule,
    MongooseModule.forFeature([{ name: Marvel.name, schema: MarvelSchema }])
  ],
  controllers: [MarvelController],
  providers: [MarvelService]
})
export class MarvelModule {}
