const numberChars:number = 3; 

export const getStartTex = (text:string) =>{
    return text.slice(0,numberChars).toLocaleLowerCase();
}

export const getEndTex = (text:string) =>{
    return text.slice(text.length - numberChars, text.length).toLocaleLowerCase();
}