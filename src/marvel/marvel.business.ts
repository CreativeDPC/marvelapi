import { ParticipationCharactersDto } from './dto/participation-characters.dto';
import { ColaboratorEnum } from './enums/colaborator.enum';
import { ColaboratorsListDto } from './dto/colaborator-list.dto';
import { Comic } from './entities/comic.entity';

export const getColaboratorsByComics = (listComics:Array<Comic>):ColaboratorsListDto => {
    let colaboratorsDto: ColaboratorsListDto = new ColaboratorsListDto();

    listComics.forEach(comic =>{
        const {items} = comic.creators;
        items.forEach(item =>{
          switch (item.role) {
            case ColaboratorEnum.COLORIST:
              colaboratorsDto.colorists = [...colaboratorsDto.colorists, item.name];
              break;
            case ColaboratorEnum.EDITOR:
              colaboratorsDto.editors = [...colaboratorsDto.editors, item.name];
              break;
            case ColaboratorEnum.WRITER:
              colaboratorsDto.writers = [...colaboratorsDto.writers, item.name]
              break;
            default:
              break;
          }
        });
      });

    return colaboratorsDto;
}


export const getParticipantsByComics = (listComics:Array<Comic>):ParticipationCharactersDto => {
    let participationCharactersDto = new ParticipationCharactersDto();

    listComics.forEach(_comic =>{
        const {items} = _comic.characters;
        items.forEach(_character =>{
         let element =  participationCharactersDto.characters.find(e => e.character == _character.name)
          if(element){
            element.comics = [...element.comics, _comic.title];
          }else{
            participationCharactersDto.characters = [...participationCharactersDto.characters, { character: _character.name, comics: [ _comic.title] }];
          }
        });
      });
      
    return participationCharactersDto;
}

