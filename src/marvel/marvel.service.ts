import { RegisterRequestDto } from './dto/register-request.dto';
import { Marvel, MarvelDocument } from './schemas/marvel.schema';
import { Comic } from './entities/comic.entity';
import { getStartTex, getEndTex } from './util/string.functions';
import { Character } from './entities/character.entity';
import { getRandomNumber } from './util/random.functions';
import { HttpService, Injectable } from '@nestjs/common';
import {Md5} from "md5-typescript";
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as moment from 'moment';

@Injectable()
export class MarvelService {
  
  private PUBLIC_APIKEY:string = '6e7dbbe17582fe504dca51e151721846';
  private PRIVATE_APIKEY:string = '07f7e107d093c0209eb36c39fd23475e76f21bfc';
  private TIMESTAMP:number = getRandomNumber();
  private URL_BASE:string = 'https://gateway.marvel.com:443/v1/public'; 

  constructor(
    private httpService: HttpService,
    @InjectModel(Marvel.name) private readonly marvelModel: Model<MarvelDocument>,
  ){}

  async findCharacter(nameCharacter:string): Promise<Character>{

    let character:Character = new Character();
    const initText = getStartTex(nameCharacter);
    
    await this.httpService.get(`${this.URL_BASE}/characters?ts=${this.TIMESTAMP}&nameStartsWith=${initText}${this.getEndUrlMarvel()}`).toPromise()
    .then( ({ data }) =>{
      if(data && data.data){
        let listCharacter = data.data.results as Array<Character>;
        const endText = getEndTex(nameCharacter);
        character = listCharacter.find(e => e.name.toLocaleLowerCase().endsWith(endText)) || new Character();
      }
    })
    .catch(er => character = er);
    

    return character;
  }

  async findColaboratorsByCharacterID(idCharacter: number):Promise<Array<Comic>>{
    let listComics:Array<Comic> =[];

    await this.httpService.get(`${this.URL_BASE}/characters/${idCharacter}/comics?ts=${this.TIMESTAMP}&orderBy=title${this.getEndUrlMarvel()}`).toPromise()
    .then( ({ data }) =>{
      if(data && data.data){
        listComics = data.data.results as Array<Comic>;
      }
    })
    .catch(er => console.error(er)
    );    

    return listComics;
  }

  async registerRequest(registerRequestDto: RegisterRequestDto): Promise<Marvel> {
    let element:Marvel; 
    element = await this.marvelModel.findOne(
      { character: {$eq: registerRequestDto.character}}
    ).exec();

    if(element){
      registerRequestDto.lastSync = moment().format();
      element = await this.marvelModel.updateOne(
        { character: {$eq: registerRequestDto.character}}, 
        {$set: registerRequestDto});
    }else{
      const register = new this.marvelModel(registerRequestDto);
      element = await register.save()
    }

    return element;
  }  

  async getAllSearchs(): Promise<Array<RegisterRequestDto>> {
    let s = await this.marvelModel.find().exec() as Array<Marvel>;
    return s.map(e => new RegisterRequestDto(e) );
  }  

  private getEndUrlMarvel():string{
    const md5hash = Md5.init(`${this.TIMESTAMP}${this.PRIVATE_APIKEY}${this.PUBLIC_APIKEY}`);
    return `&apikey=${this.PUBLIC_APIKEY}&hash=${md5hash}`
  }

}
