export enum ColaboratorEnum {
    EDITOR = 'editor',
    WRITER = 'writer',
    COLORIST = 'colorist'
}