import { RegisterRequestDto } from './dto/register-request.dto';
import { Comic } from './entities/comic.entity';
import { ParticipationCharactersDto } from './dto/participation-characters.dto';
import { Character } from './entities/character.entity';
import { Controller, Get, Post, Body, Put, Param, Delete, HttpException, HttpStatus, NotFoundException } from '@nestjs/common';
import { MarvelService } from './marvel.service';
import { ColaboratorsListDto } from './dto/colaborator-list.dto';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { getColaboratorsByComics, getParticipantsByComics } from './marvel.business';
import { SearchType } from './enums/search-type.enum';
import { Marvel } from './schemas/marvel.schema';
import * as moment from 'moment';

@ApiTags('Marvel')
@Controller('marvel')
export class MarvelController {
  constructor(private readonly marvelService: MarvelService) {}

  @ApiOperation({ summary: 'Obtiene los editores, escritores y coloristas de los comics en los que aparece el personaje buscado.' })
  @Get('colaborators/:nameCharacter')
  async findColaborators(@Param('nameCharacter') nameCharacter: string):Promise<ColaboratorsListDto | HttpException> {
    
    let colaboratorsDto = new ColaboratorsListDto();
    
    try {
    
      const character:Character = await this.marvelService.findCharacter(nameCharacter);

      if(character.id){
        const listComics:Array<Comic> = await this.marvelService.findColaboratorsByCharacterID(character.id);
        colaboratorsDto = getColaboratorsByComics(listComics);

        let result:Marvel = await this.marvelService.registerRequest({
          character: nameCharacter,
          searcType: SearchType.COLABORATORS
        } as RegisterRequestDto);

        colaboratorsDto.last_sync = moment(result.lastSync).format('DD/MM/YYYY HH:mm:ss') ;
      }else{
        return new NotFoundException('No existe información para el personaje solicitado.');
      }

    } catch (error) {
      
      console.error(error);    
    }

    return colaboratorsDto;
  }

  @ApiOperation({ summary: 'Obtiene los personajes que aprecen con el personaje buscado y los comics dónde aparecen.' })
  @Get('characters/:nameCharacter')
  async findParticipants(@Param('nameCharacter') nameCharacter: string) {
    let participationCharactersDto = new ParticipationCharactersDto();
    try {
      const character:Character = await this.marvelService.findCharacter(nameCharacter);

      if(character.id){
        const listComics = await this.marvelService.findColaboratorsByCharacterID(character.id);
        participationCharactersDto = getParticipantsByComics(listComics);

        let result:Marvel = await this.marvelService.registerRequest({
          character: nameCharacter,
          searcType: SearchType.PARTICIPANTS
        } as RegisterRequestDto);

        participationCharactersDto.last_sync = moment(result.lastSync).format('DD/MM/YYYY HH:mm:ss');
      }else{
        return new NotFoundException('No existe información para el personaje solicitado.');
      }      

    } catch (error) {
      console.error(error); 
    }

    return participationCharactersDto;
  }

  @ApiOperation({ summary: 'Obtiene la información del personaje buscado' })
  @Get('character/:nameCharacter')
  async findCharacter(@Param('nameCharacter') nameCharacter: string) {
    let character:Character;
    try {
      character = await this.marvelService.findCharacter(nameCharacter);

      if(!character.id){
        return new NotFoundException('No existe información para el personaje solicitado.');
      }

    } catch (error) {
      console.error(error); 
    }
    return character;
  }

  @ApiOperation({ summary: 'Obtiene los registros de la BD.' })
  @Get('log')
  async getSearchs():Promise<Array<RegisterRequestDto>> {
    let searchs:Array<RegisterRequestDto> = new Array<RegisterRequestDto>();
    try {
      searchs = await this.marvelService.getAllSearchs();
      searchs = searchs.map(e => {
        e.lastSync = moment(e.lastSync).format('DD/MM/YYYY HH:mm:ss');
        return e;
      });
    } catch (error) {
      console.error(error); 
    }
    return searchs;
  }

}
