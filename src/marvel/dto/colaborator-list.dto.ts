import { ApiProperty } from "@nestjs/swagger";

export class ColaboratorsListDto {
    last_sync:Date | string;
    editors:Array<string>;
    writers:Array<string>;
    colorists:Array<string>;

    constructor(data:any = {}){
        this.last_sync = data.last_sync || null;
        this.editors = data.editors || [];
        this.writers = data.writers || [];
        this.colorists = data.colorists || [];
    }
}
