import { ApiProperty } from '@nestjs/swagger';

export class ParticipationCharactersDto {
    last_sync:Date | string;
    characters:Array<ParticipationCharactersItemDto>;

    constructor(data:any = {}){
        this.last_sync = data.last_sync || null;
        this.characters = data.characters || new Array<ParticipationCharactersItemDto>();
    }
}

class ParticipationCharactersItemDto{
    character:string;
    comics:Array<string>;
    constructor(data:any = {}){
        this.character = data.character || null;
        this.comics = data.comics || [];
    }
}
