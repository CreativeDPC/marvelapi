import { SearchType } from './../enums/search-type.enum';
export class RegisterRequestDto {
    lastSync:string;
    searcType:SearchType;
    character:string;

    constructor(data:any = {}){
        this.lastSync = data.lastSync || null;
        this.searcType = data.searcType || SearchType.COLABORATORS;
        this.character = data.character || '';
    }
}