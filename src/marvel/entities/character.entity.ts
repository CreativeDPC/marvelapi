export class Character {
    id: number;
    name: string;
    description: string;
    modified: Date;
    thumbnail: any;
    resourceURI: string;
    comics: any;
    series: any;
    stories: any;
    events: any;
    urls: Array<any>
  }