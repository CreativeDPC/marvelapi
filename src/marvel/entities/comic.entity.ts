export class Comic {
    id: number;
    digitalId: number;
    title: string;
    issueNumber: number;
    variantDescription: string;
    description: string;
    modified: Date;
    isbn: string;
    upc: number;
    diamondCode: string;
    ean: string;
    issn: string;
    format: string;
    pageCount: number;
    textObjects: any;
    resourceURI: string;
    urls: any;
    series: any;
    variants: Array<any>;
    collections: Array<any>
    collectedIssues: Array<any>;
    dates: Array<any>;
    prices: Array<any>;
    thumbnail: any;
    images: Array<any>;
    creators: any;
    characters: any;
    stories: any;
    events: any
  }