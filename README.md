
## Contácto

Daniel Pérez Cabrera    
Cel: 55 1120 0160    
Email:  creativedesing.dpc@gmail.com

## Descripción

API Rest que consulta los comics de Marvel por nombre de personaje.    


## Instalación

```bash
$ npm install
```

## Ejecución

```bash
# Desarrollo
$ npm run start

# Modo Watch
$ npm run start:dev

```

## End points

```bash
# Swagger
/api

# Obtiene los editores, escritores y coloristas de los comics en los que aparece el personaje buscado.
/marvel/colaborators/{nameCharacter}

# Obtiene los personajes que aprecen con el personaje buscado y los comics dónde aparecen.
/marvel/characters/{nameCharacter}

# Obtiene la información del personaje buscado
/marvel/character/{nameCharacter}

# Obtiene los registros de la BD
/marvel/log

```

## Tecnologías Utilizadas

- [Nest Js](https://nestjs.com/) (Javascript)
- Mongo DB
- [Marvel API Developer](https://developer.marvel.com/)

## Prueba en vivo

[API Marvel by Daniel Pérez](https://marvel-danielpc.herokuapp.com/api/)

## Imagenes

![Swagger](https://bitbucket.org/CreativeDPC/marvelapi/raw/b94d3cac9c8f3f3163115749261e325ce2147938/images/swagger.png)

![Colaboradores](https://bitbucket.org/CreativeDPC/marvelapi/raw/b94d3cac9c8f3f3163115749261e325ce2147938/images/colaboradores.png)

![Personajes](https://bitbucket.org/CreativeDPC/marvelapi/raw/b94d3cac9c8f3f3163115749261e325ce2147938/images/personajes.png)

![Personaje](https://bitbucket.org/CreativeDPC/marvelapi/raw/b94d3cac9c8f3f3163115749261e325ce2147938/images/datos.png)
